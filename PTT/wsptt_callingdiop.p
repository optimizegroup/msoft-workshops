define variable httcalculator as handle no-undo.
define variable bhttcalculator as handle no-undo.

create temp-table httCalculator.
httCalculator:add-new-field('getal1','integer').
httCalculator:add-new-field('getal2','integer').
httCalculator:add-new-field('resultaat','integer').

httCalculator:temp-table-prepare('ttCalculator').

bhttcalculator = httcalculator:default-buffer-handle.

bhttcalculator:buffer-create.
assign 
  bhttcalculator::getal1 = 5
  bhttcalculator::getal2 = 10.

run wsptt_calculatordiop.p (input-output table-handle httcalculator bind).


if bhttcalculator:find-first('',no-lock)
then message "Totaal van " bhttcalculator::getal1 " en " bhttcalculator::getal2 " is " bhttcalculator::resultaat view-as alert-box info buttons ok.
 
delete object httcalculator.
assign bhttcalculator = ?
       httcalculator = ?.



